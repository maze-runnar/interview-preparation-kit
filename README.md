# interview-preparation-kit
Solutions to all the problems of [Interview Preparation Kit](https://www.hackerrank.com/interview/interview-preparation-kit) on HackerRank
in C++, Java and Python.

## Question solved


|Topic  | Question | CPP | JAVA | Python| 
|-------|----------|-----|------| ------|
|Warm Up Challenges | | | | |
||[Sock Merchant](https://www.hackerrank.com/challenges/sock-merchant/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup)|||[Done](/Warm-Up%20Challenges/SockMerchant.py)|
||[Counting Valleys](https://www.hackerrank.com/challenges/counting-valleys/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup)|||[Done](/Warm-Up%20Challenges/CountingValleys.py)|
||[Jumping on the Clouds](https://www.hackerrank.com/challenges/jumping-on-the-clouds/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup)|||[Done](/Warm-Up%20Challenges/JumpingOnTheClouds.py)|
||[Repeated Strings](https://www.hackerrank.com/challenges/repeated-string/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup)|||[Done](/Warm-Up%20Challenges/RepeatedStrings.py)|
|Arrays | | | | |
||[Arrays: Left Rotation](https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Arrays/array_left_rotation.cpp)|||
||[New Year Chaos](https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Arrays/new_year_chaos.cpp)|||
||[2D Arrays](https://www.hackerrank.com/challenges/2d-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Arrays/2dArrays.cpp)|||
| |[Minimum Swaps 2](https://www.hackerrank.com/challenges/minimum-swaps-2/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays) | [Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Arrays/Minimum_Swaps2.cpp) | | |
| |[Array Manipulation](https://www.hackerrank.com/challenges/crush/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays)  | [Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Arrays/Array_Manipulation.cpp) || |
|Dictionaries and Hashmaps | | | | |
||[Ransom Note](https://www.hackerrank.com/challenges/ctci-ransom-note/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps) |[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/RansomNotes.cpp)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/Ransom_Note.java)||
| |[Two Strings](https://www.hackerrank.com/challenges/two-strings/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps) | [Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/two_strings.cpp)| [Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/Two_Strings.java) | |
||[Frequency Queries](https://www.hackerrank.com/challenges/frequency-queries/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/frequency_queries.cpp)|||
||[Sherlock and Anagrams](https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/sherlock_and_anagrams.cpp)|||
||[Count Triplets](https://www.hackerrank.com/challenges/count-triplets-1/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Dictionaries%20and%20Hashmaps/count_triplets.cpp)|||
|Sorting | | | || 
|String Manipulation|||||
|Greedy Algorithms|||||
| |[Luck Balance](https://www.hackerrank.com/challenges/luck-balance/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Greedy%20Algorithm/luck_balance.cpp)|||
||[Minimum Absolute Difference in an Array](https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Greedy%20Algorithm/minimum_absolute_difference_in_an_array.cpp)|||
||[Greedy Florist](https://www.hackerrank.com/challenges/greedy-florist/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Greedy%20Algorithm/greedy_florist.cpp)|||
||[Max Min](https://www.hackerrank.com/challenges/angry-children/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Greedy%20Algorithm/max_min.cpp)|||
||[Reverse Shuffle Merge](https://www.hackerrank.com/challenges/reverse-shuffle-merge/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Greedy%20Algorithm/reverse_shuffle_merge.cpp)|||
|Search| || ||
|Dynamic Programming| ||||
|Stacks and Queues|||||
|Graphs | | | ||
||[DFS: Connected Cell in a Grid](https://www.hackerrank.com/challenges/ctci-connected-cell-in-a-grid/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=graphs)|[Done](https://github.com/maze-runnar/interview-preparation-kit/blob/master/Graphs/DFS_Connected%20Cell_in_a_Grid.cpp)|||
|Trees | | | ||
|Linked List | || ||
|Recursion and Backtracking | ||||
|Miscellaneous | ||| |

## Communication channel 
https://gitter.im/  <br/>
user name: maze-runnar

### Contribution Guidelines
https://www.hackerrank.com/interview/interview-preparation-kit  <br/>
Go to this link and solve the problems in java and c++ put code in corresponding folder. Please update the readme with the completed question's link in the appropriate format, and then add the section completed's link in the master repo. 
